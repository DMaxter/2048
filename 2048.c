#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void StartRand(){
	long ultime;
	time(&ultime);
	srand((unsigned) ultime);
}

void PrintLogo(){
	/* Print game logo */
	printf(" ___   ___  _  _   ___\n");  
	printf("|__ \\ / _ \\| || | / _ \\\n"); 
	printf("   ) | | | | || || (_) |\n");
	printf("  / /| | | |__  | > _ <\n");
	printf(" / /_| |_| |  | || (_) |\n");
	printf("|____|\\___/   |_| \\___/\n\n");
}

void ShowBoard(int (*board)[4]){
	/* Display the game on the screen */
	int lines = 0, passes, i = 0, j = 0;
	while(lines < 4){
		printf("+---------+---------+---------+---------+\n");
		printf("|         |         |         |         |\n");
		passes = 4;
		j = 0;
		while(passes){
			if(board[i][j] == 0){
				printf("|         ");
			}else{
				printf("|  %5d  ", board[i][j]);
			}
			j++;
			passes--;
		}
		printf("|\n");
		printf("|         |         |         |         |\n");
		i++;
		lines++;
	}
	printf("+---------+---------+---------+---------+\n");
}

int IsOccupied(int (*board)[4], int i, int j){
	/* Check if the block on the board has already a number */
	return board[i][j] != 0;
}

int HasWon(int (*board)[4]){
	/* Check if there's a block with value 2048 */
	int i, j;
	for(i = 0; i < 4; i++){
		for(j = 0; j < 4; j++){
			if(board[i][j] == 2048){
				return 1;
			}
		}
	}
	return 0;
}

int IsImpossible(int (*board)[4]){
	/* Check if there are no available moves */
	int i, j;
	for(i = 0; i < 4; i++){
		for(j = 0; j < 4; j++){
			if(!IsOccupied(board, i, j)){
				return 0;
			}
		}
	}
	return 1;
}

void AddBlock(int (*board)[4]){
	/* Add random block on the board */
	int val, done = 0, i, j;
	val = ((rand()%2)+1)*2;
	while(!done){
		i = rand()%4;
		j = rand()%4;
		if(!IsOccupied(board, i, j)){
			done = 1;
			board[i][j] = val;
		}
	}
}

void Move(int (*board)[4], int dir, int *points){
	/* Move the block in the chosen direction */
	int i, j;
	if(dir == 2){ // Down
		for(i = 0; i < 4; i++){
			for(j = 3; j > 0; j--){
				if(board[j][i] == 0 && board[j-1][i] != 0){
					board[j][i] = board[j-1][i];
					board[j-1][i] = 0;
					j += 2; // To move the block another time if possible (but not join with the previous)
					if(j > 4){
						j = 4;
					}
				}else if(board[j][i] == board[j-1][i]){
					board[j-1][i] = 0;
					board[j][i] *= 2;
					(*points) += board[j][i];
				}
			}
		}
	}else if(dir == 4){ // Left
		for(i = 0; i < 4; i++){
			for(j = 0; j < 3; j++){
				if(board[i][j] == 0 && board[i][j+1] != 0){
					board[i][j] = board[i][j+1];
					board[i][j+1] = 0;
					j -= 2; // To move the block another time if possible (but not join with the previous)
					if(j < -1){
						j = -1;
					}
				}else if(board[i][j] == board[i][j+1]){
					board[i][j+1] = 0;
					board[i][j] *= 2;
					(*points) += board[i][j];
				}
			}
		}
	}else if(dir == 6){ // Right
		for(i = 0; i < 4; i++){
			for(j = 3; j > 0; j--){
				if(board[i][j] == 0 && board[i][j-1] != 0){
					board[i][j] = board[i][j-1];
					board[i][j-1] = 0;
					j += 2; // To move the block another time if possible (but not join with the previous)
					if(j > 4){
						j = 4;
					}
				}else if(board[i][j] == board[i][j-1]){
					board[i][j-1] = 0;
					board[i][j] *= 2; 
					(*points) += board[i][j];
				}
			}
		}
	}else if(dir == 8){ // Up
		for(i = 0; i < 4; i++){
			for(j = 0; j < 3; j++){
				if(board[j][i] == 0 && board[j+1][i] != 0){
					board[j][i] = board[j+1][i];
					board[j+1][i] = 0;
					j -= 2; // To move the block another time if possible (but not join with the previous)
					if(j < -1){
						j = -1;
					}
				}else if(board[j][i] == board[j+1][i]){
					board[j+1][i] = 0;
					board[j][i] *= 2;
					(*points) += board[j][i];
				}
			}
		}
	}
}

void ClearBoard(int (*board)[4]){
	/* Clean all numbers on the board */
	int i, j;
	for(i = 0; i < 4; i++){
		for(j = 0; j < 4; j++){
			board[i][j] = 0;
		}
	}
}

void main(){
	/* Game */
	StartRand();
	int board[4][4] = {0}, play = 1, score = 0, best = 0, win = 0, end = 0;
	char c;

	PrintLogo();
	printf("Dica: Joga com o NUMPAD, apenas os numeros 2, 4, 6, 8 serao aceites\n\n\n");
	printf("Prime <ENTER> para jogar...");
	getchar();

	while(play){
		AddBlock(board);
		printf("Melhor: %d\n", best);
		printf("Pontuacao: %d\n", score);
		ShowBoard(board);
		do{
			printf("Movimento: ");
			scanf(" %c", &c);
			fflush(stdin);
			c -= '0';
		}while(c != 2 && c != 4 && c != 6 && c != 8);

		Move(board, c, &score);

		if(HasWon(board) && win == 0){
			win = 1;
			printf("\nParabens! Ganhaste! Queres continuar este jogo?\n");
			do{
				scanf(" %c", &c);
				fflush(stdin);
			}while(c != 'n' && c != 'N' && c != 's' && c != 'S');
			
			if(c == 'N' || c == 'n'){
				end = 1;
				printf("\nO jogo terminou!\n");
			}
		}else if(IsImpossible(board)){
			printf("\nO jogo terminou!\n");
			end = 1;
		}

		if(end){
			if(score > best){
				best = score;
			}

			printf("\nMelhor: %d\n", best);
			printf("Ultima: %d\n\n", score);

			do{
				printf("Queres jogar outra vez?\n");
				scanf(" %c", &c);
				fflush(stdin);
			}while(c != 'n' && c != 'N' && c != 's' && c != 'S');
			
			if(c == 'N' || c == 'n'){
				play = 0;
			}else{
				ClearBoard(board);
				win = 0;
				end = 0;
				score = 0;
			}
		}
	}

	printf("\n\nMelhor pontuacao: %d\n\n\n", best);
	printf("Prime <ENTER> para sair...");
	fflush(stdin);
	getchar();
}